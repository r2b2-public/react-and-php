import * as React from "react";
import "./Table.css";
type Props = {
  rows: any[];
  rowsCount: number;
};
const Table = ({ rows, rowsCount }: Props) => {
  // TASK: make a button and fetch persons's homeworld on click. You can fetch directly SW api (https://swapi.dev/documentation#planets) if you want.
  return <table className="Table">{/* rows and columns here*/}</table>;
};
export const DummyTable = () => {
  return (
    <table>
      <thead>
        <tr>
          <th>name</th>
          <th>birth_year</th>
          <th>eye_color</th>
          <th>gender</th>
          <th>hair_color</th>
          <th>height</th>
          <th>homeworld</th>
          <th>mass</th>
          <th>skin_color</th>
          <th>created</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>D'first</td>
          <td>1 BBY</td>
          <td>red</td>
          <td>N/A</td>
          <td>blue</td>
          <td>202cm</td>
          <td>N/A</td>
          <td>100kg</td>
          <td>purple</td>
          <td>2014-12-09T13:50:51.644000Z</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colSpan={10} style={{ textAlign: "center" }}>
            number of rows: 1
          </td>
        </tr>
      </tfoot>
    </table>
  );
};

export default Table;
