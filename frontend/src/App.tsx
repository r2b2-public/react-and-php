import * as React from "react";
import "./App.css";
import Spinner from "./Spinner";
import Table, { DummyTable } from "./Table";

type Data = {
  count: number;
  rows: any[];
};
function App() {
  const [data, setData] = React.useState<Data>({ count: 0, rows: [] });

  React.useEffect(() => {
    // TASK: add `dummy` query parameter
    fetch("/api/rows.php")
      .then((r) => r.json())
      // HINT: API is returning `data.results` but we want `data.rows`
      .then(setData);
  }, []);

  return (
    <div className="App">
      <h1>SW table demo</h1>
      <h2>Dummy table here</h2>
      {/* // HINT: notify use that we are fetching data. for example with a spinner */}
      <div className="App-table-container">
        <Spinner />
        <DummyTable />
      </div>
      <hr />
      <h2>Your table here</h2>
      <div>
        <Table rowsCount={data.count} rows={data.rows} />
      </div>
    </div>
  );
}

export default App;
